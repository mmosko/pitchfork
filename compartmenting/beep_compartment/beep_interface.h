#include "compart_api.h"

#define NO_COMPARTS 2
extern struct compart comparts[NO_COMPARTS];

extern struct extension_id *ioctl_eviocgsnd_ext;
extern struct extension_id *ioctl_kiocsound_ext;
extern struct extension_id *write_ext;
extern struct extension_id *open_ext;

struct extension_data ext_ioctl_eviocgsnd_to_arg(int fd);
int ext_ioctl_eviocgsnd_from_arg(struct extension_data data);
struct extension_data ext_ioctl_eviocgsnd_to_resp(int fd);
int ext_ioctl_eviocgsnd_from_resp(struct extension_data data);
struct extension_data ext_ioctl_eviocgsnd(struct extension_data data);
struct extension_data ext_ioctl_kiocsound_to_arg(int console_fd, int period);
void ext_ioctl_kiocsound_from_arg(struct extension_data data, int *fake_fd, int *ioctl_arg);
struct extension_data ext_ioctl_kiocsound(struct extension_data data);
struct extension_data ext_write_to_arg(int fd, const void *buf, size_t count);
void ext_write_from_arg(struct extension_data data, int *fd, char **buf, size_t *count);
struct extension_data ext_write(struct extension_data data);
struct extension_data ext_open_to_arg(const void *pathname, int flags);
void ext_open_from_arg(struct extension_data data, char **pathname, int *flags);
struct extension_data ext_open(struct extension_data data);
