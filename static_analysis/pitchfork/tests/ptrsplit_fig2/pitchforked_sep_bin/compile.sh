#!/bin/sh

rm src/vuln
make -C src CFLAGS="-DPITCHFORK_DBGSTDOUT -DINCLUDE_PID -I /home/nik/chopflow/compartmenting/libcompart/ -I /home/nik/chopflow/compartmenting/libcompart_serializer /home/nik/chopflow/compartmenting/libcompart/compart.o /home/nik/chopflow/compartmenting/libcompart_serializer/libcompart_serialisation.o"
cp src/vuln vuln_main

rm src/vuln
make -C src CFLAGS="-DCOMPART_CLASS -DPITCHFORK_DBGSTDOUT -DINCLUDE_PID -I /home/nik/chopflow/compartmenting/libcompart/ -I /home/nik/chopflow/compartmenting/libcompart_serializer /home/nik/chopflow/compartmenting/libcompart/compart.o /home/nik/chopflow/compartmenting/libcompart_serializer/libcompart_serialisation.o"
cp src/vuln vuln_classified
