import sys

sys.path.insert(0, '../../../')

import function_serializer

output_file = sys.argv[1]

def InsertLines(lines, start, new_lines):
	new_lines = new_lines.split('\n')
	for i, new_line in enumerate(new_lines):
		lines.insert(start + i, new_lines[i])

interface_header_generated_code, interface_generated_code = function_serializer.RunSerializer(['redis.c'], {'exclude_to_resp' : False, 'exclude_from_resp': False, 'exclude_ext_name': False, 'to_arg_name': None, 'from_arg_name': None, 'include_boilerplate': True})

interface_libcompart_code = '''
#include <string.h>
#include <stdlib.h>

struct extension_data {
    size_t bufc;
    char buf[512];
};

#include "ae.h"
extern void beforeSleep(struct aeEventLoop *eventLoop);

'''
#add the libcompart code
interface = interface_libcompart_code + interface_header_generated_code + interface_generated_code

lines = interface.split('\n')



interface_code = '\n'.join(lines)

#write the interface code

with open(output_file, 'w') as f:
	f.write(interface_code)

