/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#pragma once

#include "clang/AST/Stmt.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/Tooling/Refactoring.h"

#include "PFOption.h"

namespace pitchfork {
namespace PFHelper {
using namespace clang;

std::string summarise_sourcecode(clang::LangOptions LO, SourceManager *SM,
                                 unsigned width, const Stmt *s);

std::string summarise_sourcecode(clang::LangOptions LO, SourceManager *SM,
                                 unsigned width, const Decl *s);

const Stmt *locate_stmt(unsigned sought_line_no, SourceManager *SM,
                        std::queue<const Stmt *> &continuation);

const Stmt *locate_stmt(unsigned sought_line_no, SourceManager *SM,
                        const Stmt *stmt);
}  // namespace PFHelper
}  // namespace pitchfork
