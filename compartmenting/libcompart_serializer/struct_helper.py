#PROTOTYPE - Henry Zhu 6/6/19

import struct_definition

import struct_data_structures
from struct_definition import Struct
from struct_definition import StructMember

from function_definition import Function

import clang.cindex

import re
import os

DEBUG = False

# use new version of marshalling by default
#MARSHALL_CODE_VERSION = 1
MARSHALL_CODE_VERSION = 2

def SET_MARSHALL_CODE_VERSION(version):
    global MARSHALL_CODE_VERSION
    MARSHALL_CODE_VERSION = version

def log(s):
    if DEBUG:
        print s


#remove all unnecessary typedefs, or structural aliass until the base struct is found
def LoopUntilFoundBaseStructNode(node):
    children = node.get_children()
    for c in children:
        definition = c.get_definition()
        if definition and definition.kind == clang.cindex.CursorKind.STRUCT_DECL:
            print "YES" + definition.spelling
            return c
        next_loop = LoopUntilFoundBaseStructNode(c)
        if next_loop is not None:
            return next_loop
    return None


#check if node is a struct
def VerifyIfStructNode(node):
    definition = node.get_definition()

    #struct with no name
    if definition.spelling == '':
        return None

    if definition.kind == clang.cindex.CursorKind.STRUCT_DECL:
        return Struct(node)
    #check for typedef to structs
    elif definition.kind == clang.cindex.CursorKind.TYPEDEF_DECL:
        if not node.is_definition():
            node = node.get_definition()
        base_struct_node = LoopUntilFoundBaseStructNode(node)
        if base_struct_node is not None:
            return Struct(base_struct_node, node)

        canonical_node = node.canonical
        if definition and definition.kind == clang.cindex.CursorKind.STRUCT_DECL:
            return Struct(canonical_node, node)
        # For typedef structs, the get_childeren() method is returning None
        # So right now, we are assuming that if definition.kind is TYPEDEF_DECL,
        # then it must be a 'typedef struct' and not 'typedef primtive_type'
        # so we just return the following
        '''
        print canonical_node
        if node.kind.is_declaration():
            a = node.underlying_typedef_type
        for c in node.get_children():
            definition = c.get_definition()
            if definition is not None:
                print "" + " " + definition.spelling + " " + str(definition.kind)
        '''
    return None


#returns a mapping of {name, struct}
def GetStructs(tu):
    structs = {}
    for node in tu.cursor.get_children():
        definition = node.get_definition()
        if definition:
            struct = VerifyIfStructNode(node)
            if struct:
                name = struct.GetName()
                structs[name] = struct

    return structs


# loop through each struct member field and get the base type
def ParseStructMembers(structs, struct_members, node):
    definition = node.get_definition()
    if definition:
        #check if this current iteration references another struct
        #libclang doesn't go into another layer
        #print definition.spelling, definition.type.kind, definition.type.spelling
        member = StructMember(node)
        struct_members[member.GetName()] = member

        children = list(node.get_children())
        if len(children) > 0:
            #assume this is only one (aka another struct)
            #assert(len(children) == 1)
            node = children[0]
            definition = node.get_definition()

            #can be typedef or struct
            if definition:
                if definition.type.kind == clang.cindex.TypeKind.RECORD or definition.type.kind == clang.cindex.TypeKind.TYPEDEF:
                    struct_reference = structs.get(definition.spelling)
                    #TODO check if this struct references itself
                    #can't just check if it references current struct
                    #cause it can be circular
                    #one way is to keep track of a set of structs already parsed (?)
                    if struct_reference:
                        member.SetStructReference(definition.spelling)
                    else:
                        print 'WARNING could not find struct reference in parsing members:', definition.spelling

    else:
        print 'WARNING parsing definition is not valid:', definition


def ParseStructs(structs):
    print ''
    print 'Parsing structs...'
    for name, struct in structs.items():
        node = struct.GetNode()
        for c in node.get_children():
            ParseStructMembers(structs, struct.GetMembers(), c)


def PrintStructNames(structs):
    for name, struct in structs.items():
        print name
        for _, member in struct.GetMembers().items():
            print '  ' + member.GetName() + ' : ' + member.GetType()


#Get user input for what struct to request
def GetStructInput(structs):
    struct = None
    while struct is None:
        print 'Select struct to parse'
        PrintStructNames(structs)
        parse_struct_name = raw_input('Enter struct to parse: ')

        if parse_struct_name not in structs:
            print 'Please input a valid struct'
        else:
            struct = structs[parse_struct_name]

    return struct


def ConvertToInt(s):
    try:
        return int(s)
    except ValueError:
        return None


class PointerRefType():
    STRING = 0
    INTEGER = 1
    STRUCT_MEMBER_REFERENCE = 2
    ARGUMENT = 3
    PRIMITIVE = 4
    CUSTOM = 5


# wrapper around a pointer and its type
class PointerRef():
    def __init__(self, pointer_ref_type, data):
        self.pointer_ref_type = pointer_ref_type
        self.data = data

    def GetType(self):
        return self.pointer_ref_type

    def GetData(self):
        return self.data

#Get user input for number of items the data type has
def GetMemberSizeInput(struct,
                       member,
                       member_name,
                       p_path=[],
                       p_sub_path=[],
                       mtype=None,
                       can_be_string=True):
    struct_name = struct.GetName()
    members = struct.GetMembers()
    member_name = member.GetName()
    if not mtype:
        mtype = member.GetType()

    print 'Enter \'$\' to assume NULL-terminated string'
    print 'Enter a number like \'3\' for fixed size array'
    print 'Enter a member like \'#\' to select a member variable to be the type'
    print 'Enter a member like \'@\' to add an argument \'num elements\' to passed to this struct serialiation for this function'
    print 'Enter a member like \'=\' to copy the pointer as a primitive'
    print 'Enter a member like \'^\' to marshall a data structure'
    print 'Enter a member like \'!\' to insert custom marshalling code'
    print 'Press enter for default (either string copy if char *, else copy as primitive)'
    print 'Struct', struct_name
    print 'Member:', mtype, member_name

    result = None
    while not result:
        result = raw_input('Enter: \n')
        integer = ConvertToInt(result)
        if integer:
            if integer > 0:
                return PointerRef(PointerRefType.INTEGER, integer)
            else:
                print 'Integer cannot be negative', integer
                return None
        if mtype == 'char *' and (result == '' or result == '$'):
            member_name_str = str(member_name)
            p_sub_path.append(member_name_str)
            p_path.append(p_sub_path[:])
            buf = p_sub_path.pop()
            return PointerRef(PointerRefType.STRING, result)
        if result == "#":
            #list out members
            for member_name, member in members.items():
                mtype = member.GetType()
                print mtype + ' ' + member_name

            #ask and verify that the member is in struct
            result = raw_input('Enter: \n')
            if result in members:
                return PointerRef(PointerRefType.STRUCT_MEMBER_REFERENCE,
                                  result)
            else:
                print 'The member is not in the struct', result
        if result == "@":
            #ask for type
            arg_type = raw_input('Enter argument type: \n')
            arg_name = raw_input('Enter argument name: \n')
            return PointerRef(PointerRefType.ARGUMENT, [arg_type, arg_name])
        if result == '' or result == "=":
            return PointerRef(PointerRefType.PRIMITIVE, None)
        if result == '^':
            print 'Here is a list of data structures to select from:'
            if MARSHALL_CODE_VERSION == 2:
                path_to_data_structures = os.path.join(os.path.dirname(__file__), 'data_structures')
                data_structures = struct_data_structures.get_data_structures(path_to_data_structures)
                for name in data_structures.keys():
                    print name
                selection = raw_input('Selection: \n')
                selected_data_structure = data_structures.get(selection)
                if selected_data_structure != None:
                    for replacement in selected_data_structure.get_replacements():
                        answer = raw_input('Input ' + replacement + '\n')
                        selected_data_structure.fill_replacement(replacement, answer)
                    custom_code = selected_data_structure.get_resolved_code(struct.GetBaseType(), member.GetName())
                    return PointerRef(PointerRefType.CUSTOM, [custom_code, 1])
            elif MARSHALL_CODE_VERSION == 1:
                data_structures = struct_data_structures.data_structures
                for name in data_structures:
                    print name
                selection = raw_input('Selection: \n')
                selected_data_structure = data_structures.get(selection)
                if selected_data_structure != None:
                    if selection == 'singular_linked_list':
                        # this p_sub_path is to indicate that we found a data structure, same below
                        p_sub_path.insert(0, '^ 1')
                        p_sub_path.insert(1, str(mtype))
                        member_name_str = str(member_name)
                        p_sub_path.append(member_name_str)
                        custom_code = selected_data_structure
                        custom_code = re.sub(r'%NODE_DATA_TYPE%',
                                             struct.GetBaseType(), custom_code)
                        custom_code = re.sub(r'%NEXT_NODE%', member.GetName(),
                                             custom_code)
                        return PointerRef(PointerRefType.CUSTOM, [custom_code, 1])
                    elif selection == 'circular_linked_list':
                        p_sub_path.insert(0, '^ 2')
                        p_sub_path.insert(1, str(mtype))
                        member_name_str = str(member_name)
                        p_sub_path.append(member_name_str)
                        custom_code = selected_data_structure
                        custom_code = selected_data_structure
                        custom_code = re.sub(r'%NODE_DATA_TYPE%',
                                             struct.GetBaseType(), custom_code)
                        custom_code = re.sub(r'%NEXT_NODE%', member.GetName(),
                                             custom_code)
                        prev_member_name = raw_input('Input prev member name: ')
                        custom_code = re.sub(r'%PREV_NODE%', prev_member_name,
                                             custom_code)
                        return PointerRef(PointerRefType.CUSTOM, [custom_code, 1])
                    elif selection == 'doubly_linked_list':
                        p_sub_path.insert(0, '^ 3')
                        p_sub_path.insert(1, str(mtype))
                        member_name_str = str(member_name)
                        p_sub_path.append(member_name_str)
                        custom_code = selected_data_structure
                        custom_code = selected_data_structure
                        custom_code = re.sub(r'%NODE_DATA_TYPE%',
                                             struct.GetBaseType(), custom_code)
                        custom_code = re.sub(r'%NEXT_NODE%', member.GetName(),
                                             custom_code)
                        prev_member_name = raw_input('Input prev member name: ')
                        custom_code = re.sub(r'%PREV_NODE%', prev_member_name,
                                             custom_code)
                        return PointerRef(PointerRefType.CUSTOM, [custom_code, 1])
                    elif selection == 'binary_tree':
                        p_sub_path.insert(0, '^ 4')
                        p_sub_path.insert(1, str(mtype))
                        member_name_str = str(member_name)
                        p_sub_path.append(member_name_str)
                        custom_code = selected_data_structure
                        custom_code = selected_data_structure
                        custom_code = re.sub(r'%NODE_DATA_TYPE%',
                                             struct.GetBaseType(), custom_code)
                        custom_code = re.sub(r'%LEFT_NODE%', member.GetName(),
                                             custom_code)
                        prev_member_name = raw_input('Input right member name: ')
                        custom_code = re.sub(r'%RIGHT_NODE%', prev_member_name,
                                             custom_code)
                        p_sub_path.append(prev_member_name)
                        return PointerRef(PointerRefType.CUSTOM, [custom_code, 1])
        if result == '!':
            print 'Enter custom marshalling code: \n\'data\' is the struct* and %MARSHALL(...)% means to marshall this member variable'
            print 'Type \'DONE\' when finished'
            custom_code = ''
            next_line = ''
            while True:
                next_line = raw_input('')
                if next_line == 'DONE':
                    break
                custom_code += next_line + '\n'

            integer = raw_input(
                'Enter number of times the element should be marshalled: \n')
            integer = ConvertToInt(integer)
            if integer:
                if integer > 0:
                    return PointerRef(PointerRefType.CUSTOM,
                                      [custom_code, integer])
                else:
                    print 'Integer cannot be negative', integer
                    return None
        result = None
        print 'Invalid input -- Input another selection'

    return result


# the marshall generator code prologue
def BeginMarshallFunctionTemplate(struct):
    struct_type = struct.GetType()
    struct_type_no_space = struct.GetTypeNoSpace()

    marshall_prototype = 'void marshall_%s(char* buf, size_t* buf_index_, %s* TEST_data)' % (
        struct_type_no_space, struct_type)
    marshall_function = Function(marshall_prototype)
    marshall_begin = '''
  size_t buf_index = *buf_index_;

  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
'''
    marshall_function.AddCLines(marshall_begin)

    return marshall_function


# the marhsall generator code epilogue
def EndMarshallFunctionTemplate(marshall_function):
    marshall_end = '''
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }

  *buf_index_ = buf_index;
'''

    marshall_function.AddCLines(marshall_end)
