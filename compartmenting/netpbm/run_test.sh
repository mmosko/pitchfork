#!/bin/bash
set -e

PREFIX=/home/nik/t/chopchop/compartmenting/ 
#tifftopnm_libcompart
#tifftopnm_original
EXEC=${PREFIX}netpbm-10.73.28/converter/other/tifftopnm
FROM=/home/nik/test.tiff
TO=/home/nik/test.pnm

while getopts "e:p:vci:o:t" o; do
    case "${o}" in
        e)
          EXEC=${OPTARG}
          ;;
        p)
          PREFIX=${OPTARG}
          ;;
        v)
          VERBOSE=1
          ;;
        c)
          CHECK=1
          ;;
        i)
          FROM=${OPTARG}
          ;;
        o)
          TO=${OPTARG}
          ;;
        t)
          TIMED=1
          ;;
    esac
done
shift $((OPTIND-1))

CMD="sudo sh -c \"LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${PREFIX}/netpbm/netpbm-10.73.28/lib/:${PREFIX}/libcompart ${EXEC} --quiet ${FROM} > ${TO}\""

if [ ! -z ${VERBOSE} ]
then
  echo "FROM=${FROM}"
  ls -al ${FROM}
  echo "TO=${TO}"
  echo CMD=${CMD}
fi

rm -f ${TO}
if [ -z ${TIMED} ]
then
  eval ${CMD} || true
else
  eval command time ${CMD} || true
fi
ls -al ${TO}

if [ ! -z ${CHECK} ]
then
  TO_CHECK="${TO}.png"
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${PREFIX}/netpbm/netpbm-10.73.28/lib/ ./netpbm-10.73.28/converter/other/pnmtopng ${TO} > ${TO_CHECK}
  ls -al ${TO_CHECK}
fi
