import sys

sys.path.insert(0, '../')

import function_serializer

output_file = sys.argv[2]

interface_header_generated_code, interface_generated_code = function_serializer.RunSerializer([sys.argv[1]], {'exclude_to_resp' : False, 'exclude_from_resp': False, 'exclude_ext_name': False, 'to_arg_name': None, 'from_arg_name': None, 'include_boilerplate': True})

#write the interface code
interface_code = interface_header_generated_code + interface_generated_code
with open(output_file, 'w') as f:
	f.write(interface_code)


