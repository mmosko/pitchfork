// RUN: printf "add_ten\n" | python -B setup.py hello_vulnerable_interface.h
// RUN: %CC %C_FLAGS -I../add_function %s -o %t
// RUN: echo "" > %t.log
// RUN: sudo PITCHFORK_LOG="%t.log" %t | %FileCheck %s || true

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "add_ten.h"
#include "compart_api.h"
#include "hello_vulnerable_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);
  	add_ten_ext = compart_register_fn("other compartment", &ext_add_ten);

  	compart_start("hello compartment");

	int old_val = 0;

	struct extension_data arg = ext_add_ten_to_arg(old_val);
	int new_val = ext_add_ten_from_resp(compart_call_fn(add_ten_ext, arg));

	printf("OLD VALUE: %d, NEW VALUE: %d\n", old_val, new_val);

	//possible vulnerable code
	if(new_val >= 10)
	{
		printf("NEW VAL IS VULNERABLE\n");
	}
	else
	{
		printf("NEW VAL IS NOT VULNERABLE\n");
// CHECK: NEW VAL IS NOT VULNERABLE
	}

	fflush(stdout);
	return 0;
}
