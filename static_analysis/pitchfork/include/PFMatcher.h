/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#pragma once

#include "clang/AST/AST.h"
#include "clang/AST/Stmt.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Parse/ParseAST.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/Tooling/Refactoring.h"

#include <map>
#include <string>
#include <vector>
#include <fstream>
#include <set>
#include <utility>
#include "string.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/DiagnosticIDs.h"
#include "clang/Frontend/TextDiagnostic.h"

namespace pitchfork {
using namespace llvm;
using namespace clang;

enum class Reachability { Yes, No, Maybe, Error };

using namespace clang::ast_matchers;

class ResFunctionFinder : public MatchFinder::MatchCallback {
  std::string label;
  std::vector<const Stmt *> * ResFunctionStmts_p;
 public:
  ResFunctionFinder() = delete;
  ResFunctionFinder(std::string l, std::vector<const Stmt *> * store) : label(l), ResFunctionStmts_p(store) {};
  virtual void run(const MatchFinder::MatchResult &Result);

  StatementMatcher makeMatcher(std::string to_match);
};

}  // namespace pitchfork
