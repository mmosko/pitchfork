
#include <linux/input.h>
#include <string.h>
#include <stdlib.h>

#define NO_COMPARTS 2

static struct compart comparts[NO_COMPARTS] =
  {{.name = "struct compartment", .uid = 65534, .gid = 65534, .path = "/dev/"},
   {.name = "other compartment", .uid = 65534, .gid = 65534, .path = "/dev/"}};


struct extension_data ext_CallInputEvent(struct extension_data data);

void marshall_struct_timeval(char* buf, size_t* buf_index_, struct timeval* TEST_data);

struct extension_data ext_CallInputEvent_to_arg(int console_fd, struct input_event* e);

void _unmarshall_struct_input_event(char* buf, size_t* buf_index_, struct input_event** TEST_data);

int ext_CallInputEvent_from_resp(struct extension_data data);

struct extension_data ext_CallInputEvent_to_resp(int result);

void marshall_struct_input_event(char* buf, size_t* buf_index_, struct input_event* TEST_data);

void ext_CallInputEvent_from_arg(struct extension_data data, int *console_fd, struct input_event* *e);

void _unmarshall_struct_timeval(char* buf, size_t* buf_index_, struct timeval** TEST_data);

#define unmarshall_struct_input_event(a, b, c) 	_unmarshall_struct_input_event(a, b, &c)

#define unmarshall_struct_timeval(a, b, c) 	_unmarshall_struct_timeval(a, b, &c)

#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
void marshall_struct_timeval(char* buf, size_t* buf_index_, struct timeval* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->tv_sec);
    marshall_prim(buf, &buf_index, TEST_data->tv_usec);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_input_event(char* buf, size_t* buf_index_, struct input_event** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct input_event* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->code);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->type);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->value);
    {
      struct timeval* tmp = NULL;
      unmarshall_struct_timeval(buf, &buf_index, tmp);
      memcpy(&TEST_data_ref->time, tmp, sizeof(*tmp));
      free(tmp);
    }
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_input_event(a, b, c) 	_unmarshall_struct_input_event(a, b, &c)

void marshall_struct_input_event(char* buf, size_t* buf_index_, struct input_event* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->code);
    marshall_prim(buf, &buf_index, TEST_data->type);
    marshall_prim(buf, &buf_index, TEST_data->value);
    marshall_struct_timeval(buf, &buf_index, &TEST_data->time);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_timeval(char* buf, size_t* buf_index_, struct timeval** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct timeval* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tv_sec);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tv_usec);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_timeval(a, b, c) 	_unmarshall_struct_timeval(a, b, &c)

struct extension_data ext_CallInputEvent(struct extension_data data)
{
  int console_fd;
  struct input_event* e;
  ext_CallInputEvent_from_arg(data, &console_fd, &e);


//TODO fix, this is bug with libcompart not shutting down with return value of 0


  int return_value;
  return_value = CallInputEvent(console_fd, e);
  struct extension_data result = ext_CallInputEvent_to_resp( return_value );
  return result;
}
struct extension_data ext_CallInputEvent_to_arg(int console_fd, struct input_event* e)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, console_fd);
  marshall_struct_input_event(buf, &buf_index, e);
  data.bufc = buf_index;
  return data;
}
int ext_CallInputEvent_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
struct extension_data ext_CallInputEvent_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
void ext_CallInputEvent_from_arg(struct extension_data data, int *console_fd, struct input_event* *e)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, *console_fd);
  unmarshall_struct_input_event(buf, &buf_index, *e);
}
