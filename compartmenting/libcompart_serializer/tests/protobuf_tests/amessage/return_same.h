#pragma once

#include "amessage.pb-c.h"

#define MESSAGE_A 1
#define MESSAGE_HAS_B 1
#define MESSAGE_B 2

#define SUCCESS 1
#define NO_SUCCESS 0

int return_same(struct _AMessage* amessage)
{
    if(amessage->a == MESSAGE_A && amessage->has_b == MESSAGE_HAS_B && amessage->b == MESSAGE_B)
    {
        return SUCCESS;
    }
	return NO_SUCCESS;
}
