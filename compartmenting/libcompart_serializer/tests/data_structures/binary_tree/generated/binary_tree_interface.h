
#include "../header/binary_tree.h"
#include <string.h>
#include <stdlib.h>

#define NO_COMPARTS 2

static struct compart comparts[NO_COMPARTS] =
  {{.name = "struct compartment", .uid = 65534, .gid = 65534, .path = "/dev/"},
   {.name = "other compartment", .uid = 65534, .gid = 65534, .path = "/dev/"}};


void _unmarshall_struct_Node(char* buf, size_t* buf_index_, struct Node** TEST_data);

struct extension_data ext_IsInvalidBinaryTree_to_resp(int result);

void serialize_binary_tree(struct Node* node, int* valids, int* values, int* values_index);

void clean_up(struct Node* head);

void marshall_struct_Node(char* buf, size_t* buf_index_, struct Node* TEST_data);

void ext_IsInvalidBinaryTree_from_arg(struct extension_data data, struct Node* *head);

struct extension_data ext_IsInvalidBinaryTree_to_arg(struct Node* head);

int ext_IsInvalidBinaryTree_from_resp(struct extension_data data);

struct Node* deserialize_binary_tree(int* valids, int* values, int* values_index);

struct extension_data ext_IsInvalidBinaryTree(struct extension_data data);

#define unmarshall_struct_Node(a, b, c) 	_unmarshall_struct_Node(a, b, &c)

#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
void _unmarshall_struct_Node(char* buf, size_t* buf_index_, struct Node** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct Node* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->val);
        int values_index = 0; 
        unmarshall_prim(buf, &buf_index, values_index); 
        int* valids = calloc(sizeof(int), values_index); 
        int* values = calloc(sizeof(int), values_index); 
        unmarshall_size(buf, &buf_index, valids, values_index); 
        unmarshall_size(buf, &buf_index, values, values_index); 
        values_index = 0; 
        int* values_index_ptr = &values_index; 
        struct Node* root = deserialize_binary_tree(valids, values, values_index_ptr); 
        memcpy(TEST_data_ref, root, sizeof(struct Node)); 
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_Node(a, b, c) 	_unmarshall_struct_Node(a, b, &c)

void serialize_binary_tree(struct Node* node, int* valids, int* values, int* values_index)
{
  {
        if(node != NULL)
        {
            valids[*values_index] = 1;
            values[*values_index] = node->val;
            (*values_index)++;
            serialize_binary_tree(node->left, valids, values, values_index);
            serialize_binary_tree(node->right, valids, values, values_index);
        }
        else
        {
            valids[*values_index] = 0;
            (*values_index)++;
        }
  }
}
void marshall_struct_Node(char* buf, size_t* buf_index_, struct Node* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->val);
        int* valids = calloc(sizeof(int), 1000); 
        int* values = calloc(sizeof(int), 1000); 
        int values_index = 0; 
        int* values_index_ptr = &values_index; 
        serialize_binary_tree(TEST_data, valids, values, values_index_ptr); 
        marshall_prim(buf, &buf_index, values_index); 
        marshall_size(buf, &buf_index, valids, values_index); 
        marshall_size(buf, &buf_index, values, values_index); 
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
struct Node* deserialize_binary_tree(int* valids, int* values, int* values_index)
{
  {
        struct Node* node = calloc(sizeof(struct Node), 1);
        int valid = valids[*values_index];
        int value = values[*values_index];
        (*values_index)++;
        if(!valid) {
            return NULL;
        }
        node->val = value;
        node->left = deserialize_binary_tree(valids, values, values_index);
        node->right = deserialize_binary_tree(valids, values, values_index);
        return node;
  }
}
void clean_up(struct Node* head)
{
  if (head != NULL) free(head);
}
struct extension_data ext_IsInvalidBinaryTree_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
void ext_IsInvalidBinaryTree_from_arg(struct extension_data data, struct Node* *head)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_struct_Node(buf, &buf_index, *head);
}
struct extension_data ext_IsInvalidBinaryTree_to_arg(struct Node* head)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_struct_Node(buf, &buf_index, head);
  data.bufc = buf_index;
  return data;
}
int ext_IsInvalidBinaryTree_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
struct extension_data ext_IsInvalidBinaryTree(struct extension_data data)
{
  struct Node* head;
  ext_IsInvalidBinaryTree_from_arg(data, &head);


//TODO fix, this is bug with libcompart not shutting down with return value of 0

if(IsValidOriginalBinaryTree(head)) {
	if(IsInvalidBinaryTree(head)) {
		FreeBinaryTree(head);
		head = MakeValidBinaryTree();
	}
}


  int return_value;
  return_value = IsInvalidBinaryTree(head);
  struct extension_data result = ext_IsInvalidBinaryTree_to_resp( return_value );
  clean_up(head);
  return result;
}
