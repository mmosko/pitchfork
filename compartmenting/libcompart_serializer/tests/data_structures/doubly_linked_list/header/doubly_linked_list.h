#pragma once

#include <stdlib.h>

struct Node {
	struct Node* next;
    struct Node* prev;
	int val;
};

struct Node* AllocateNode(int val) {
	struct Node* node = calloc(sizeof(struct Node), 1);
	node->next = NULL;
    node->prev = node;
	node->val = val;

	return node;
}

void AppendNodeToBack(struct Node* head, int val) {
	struct Node* new_node = AllocateNode(val);

	// Find end
	struct Node* node = head;
	while(node->next != NULL) {
		node = node->next;
	}
	
	node->next = new_node;
    new_node->prev = node;
}

void FreeLinkedList(struct Node* head) {
	if(head->next != NULL) {
		FreeLinkedList(head->next);
	}
	free(head);
}

int IsValidOriginalLinkedList(struct Node* head) {
	if(head->val != 3) {
		return 0;
	}

	head = head->next;
	if(head->val != 2) {
		return 0;
	}

	head = head->next;
	if(head->val != 1) {
		return 0;
	}

	head = head->next;
	if(head != NULL) {
		return 0;
	}

	return 1;
}

// Check 1 -> 2 -> 3
int IsInvalidLinkedList(struct Node* head) {
	if(head->val != 1) {
		return 1;
	}

	head = head->next;
	if(head->val != 2) {
		return 1;
	}

	head = head->next;
	if(head->val != 3) {
		return 1;
	}

	return 0;
}

struct Node* MakeValidLinkedList() {
	struct Node* head = AllocateNode(1);
	AppendNodeToBack(head, 2);
	AppendNodeToBack(head, 3);
	return head;
}
