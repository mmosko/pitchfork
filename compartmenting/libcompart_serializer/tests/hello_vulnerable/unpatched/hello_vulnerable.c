// RUN: gcc -g -DINCLUDE_PID -I../add_function %s -o %t
// RUN: %t | %FileCheck %s

#include <stdio.h>

int add_ten(int val)
{
	return val + 10;
}

int main()
{
	int old_val = 0;
	int new_val = add_ten(old_val);

	printf("OLD VAL: %d, NEW VAL: %d\n", old_val, new_val);

	//possible vulnerable code
	if(new_val >= 10)
	{
		printf("NEW VAL IS VULNERABLE\n");
// CHECK: NEW VAL IS VULNERABLE
	}
	else
	{
		printf("NEW VAL IS NOT VULNERABLE\n");
	}

	return 0;
}
