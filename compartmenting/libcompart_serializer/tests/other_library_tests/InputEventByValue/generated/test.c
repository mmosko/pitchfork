// RUN: python -B setup.py input_event_interface.h
// RUN: %CC %C_FLAGS -I../header %s -o %t
// RUN: echo "" > %t.log
// RUN: sudo PITCHFORK_LOG="%t.log" %t | %FileCheck %s || true

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "input_event.h"
#include "compart_api.h"
#include "input_event_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);

	struct extension_id* struct_ext = compart_register_fn("other compartment", &ext_CallInputEvent);

  	compart_start("struct compartment");

	int console_fd = 0;
	struct input_event inputevent;
	inputevent.type = 1;
	inputevent.code = 1;
	inputevent.value = 1;

	struct extension_data arg = ext_CallInputEvent_to_arg(console_fd, inputevent);
	int result = ext_CallInputEvent_from_resp(compart_call_fn(struct_ext, arg));
	if(result > 0)
	{
		printf("VULNERABLE, GOT A RESULT OF %d\n", result);
	}
	else
	{
		printf("NOT VULNERABLE\n");
// CHECK: NOT VULNERABLE
	}

	ext_CallInputEvent_from_resp(compart_call_fn(struct_ext, arg));
	return 0;
}
