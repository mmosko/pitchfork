struct one
{
	int a;
	char b;
};
typedef struct two
{
	int a;
	char* b;
} two;
typedef struct
{
	int a;
	char* b;
} three;
typedef struct
{
	int a;
	char** b;
} four;
typedef struct
{
	int a;
	char** b;
	struct one o;
} five;
typedef struct
{
	int a;
	char** b;
	struct one* o;
} six;
typedef struct
{
	int a[2];
	char** b;
	five* o;
} seven;
