// RUN: protoc-c --proto_path=%S --c_out=. dmessage.proto 
// RUN: mv %S/dmessage.pb-c.c %S/dmessage.pb-c.cc
// RUN: %CC %C_FLAGS -I%S %s %S/dmessage.pb-c.cc -lprotobuf-c -o %t
// RUN: %t | %FileCheck %s

#include <stdio.h>
#include <stdlib.h>
#include "dmessage.pb-c.h"
#include "return_same.h"

int main(int argc, const char * argv[]) 
{
    DMessage msg = DMESSAGE__INIT;
    msg.d = some_message;
    
    unsigned len = dmessage__get_packed_size(&msg);

    void* buf = malloc(len);
    dmessage__pack(&msg, buf);
    
    DMessage* msg2 = dmessage__unpack(NULL, len, buf);
    
    printf("%s\n", msg2->d);
//CHECK: MESSAGE
    dmessage__free_unpacked(msg2, NULL);

    free(buf); // Free the allocated serialized buffer
    return 0;
}
