#include <stdbool.h>
#include "demarsh.h"
#include "demarsh.c"

void marshall_unsigned_int (struct extension_data *edata, unsigned int data);
void demarshall_unsigned_int (size_t *offset, struct extension_data *edata, unsigned int *data);

void marshall_unsigned_int (struct extension_data *edata, unsigned int data) {
  size_t size = sizeof(unsigned int);
  CHECK_BUFFER_SIZE
  memcpy(edata->buf + edata->bufc, &data, size);
  edata->bufc += size;
}

void demarshall_unsigned_int (size_t *offset, struct extension_data *edata, unsigned int *data) {
  size_t size = sizeof(unsigned int);
  CHECK_OFFSET
  memcpy(data, edata->buf + *offset, size);
  *offset += size;
}

void args_to_data_CommandLine(struct extension_data *edata, int argc, const char **argv);
void args_from_data_CommandLine(struct extension_data *edata, int *argc, char ***argv);
struct extension_data ext_parseCommandLine(struct extension_data edata);

void marshall_CmdlineInfo (struct extension_data *edata, const struct CmdlineInfo *cmdline);
void demarshall_CmdlineInfo (size_t *offset, struct extension_data *edata, struct CmdlineInfo *cmdline);
void args_to_data (struct extension_data *edata, const struct CmdlineInfo *cmdline);
void args_from_data (struct extension_data *edata, struct CmdlineInfo *cmdline);

void marshall_CmdlineInfo (struct extension_data *edata, const struct CmdlineInfo *cmdline)
{
    marshall_char_pt_null(edata, cmdline->inputFilename);
    marshall_unsigned_int(edata, cmdline->headerdump);
    marshall_char_pt_null(edata, cmdline->alphaFilename);
    marshall_bool(edata, cmdline->alphaStdout);
    marshall_unsigned_int(edata, cmdline->respectfillorder);
    marshall_unsigned_int(edata, cmdline->byrow);
    marshall_unsigned_int(edata, cmdline->orientraw);
    marshall_unsigned_int(edata, cmdline->verbose);
}

void demarshall_CmdlineInfo (size_t *offset, struct extension_data *edata, struct CmdlineInfo *cmdline)
{
    demarshall_char_pt_null(offset, edata, &cmdline->inputFilename);
    demarshall_unsigned_int(offset, edata, &cmdline->headerdump);
    demarshall_char_pt_null(offset, edata, &cmdline->alphaFilename);
    demarshall_bool(offset, edata, &cmdline->alphaStdout);
    demarshall_unsigned_int(offset, edata, &cmdline->respectfillorder);
    demarshall_unsigned_int(offset, edata, &cmdline->byrow);
    demarshall_unsigned_int(offset, edata, &cmdline->orientraw);
    demarshall_unsigned_int(offset, edata, &cmdline->verbose);
}

void args_to_data (struct extension_data *edata, const struct CmdlineInfo *cmdline)
{
    edata->bufc = 0;
    marshall_CmdlineInfo(edata, cmdline);
}

void args_from_data (struct extension_data *edata, struct CmdlineInfo *cmdline)
{
    size_t buf_index = 0;
    demarshall_CmdlineInfo(&buf_index, edata, cmdline);
}

struct extension_data ext_convertTIFF(struct extension_data edata)
{
    TIFF * tiffP;
    FILE * alphaFile;
    FILE * imageoutFile;

    struct CmdlineInfo cmdline;

    args_from_data(&edata, &cmdline);
// Start
    tiffP = newTiffImageObject(cmdline.inputFilename);

    if (cmdline.alphaStdout)
        alphaFile = stdout;
    else if (cmdline.alphaFilename == NULL) 
        alphaFile = NULL;
    else
        alphaFile = pm_openw(cmdline.alphaFilename);

    if (cmdline.alphaStdout) 
        imageoutFile = NULL;
    else
        imageoutFile = stdout;

    convertIt(tiffP, alphaFile, imageoutFile, cmdline);

    if (imageoutFile != NULL) 
        pm_close( imageoutFile );
    if (alphaFile != NULL)
        pm_close( alphaFile );

    TIFFClose(tiffP);
// Stop
    return edata;
}

void args_to_data_CommandLine(struct extension_data *edata, int argc, const char **argv)
{
    edata->bufc = 0;
    dump_marshall_int(argc, "argc : %d\n");
    marshall_int(edata, argc);
    dump_marshall_char_pt_null_pt_bounded((unsigned char)argc, argv, "argv :\n%s\n");
    marshall_char_pt_null_pt_bounded(edata, (unsigned char)argc, argv);
}

void args_from_data_CommandLine(struct extension_data *edata, int *argc, char ***argv)
{
    size_t buf_index = 0;
    size_t shadow_buf_index = 0;
    dump_demarshall_int(&shadow_buf_index, edata, "argc : %d\n");
    demarshall_int(&buf_index, edata, argc);
    dump_demarshall_char_pt_null_pt_bounded(&shadow_buf_index, edata, (unsigned char)*argc, "argv:\n%s\n");
    demarshall_char_pt_null_pt_bounded(&buf_index, edata, (unsigned char)*argc, argv);
}

struct extension_data ext_parseCommandLine(struct extension_data edata)
{
    int argc;
    const char **argv;
    struct CmdlineInfo cmdline;

    args_from_data_CommandLine(&edata, &argc, (char ***)&argv);
// Start
    parseCommandLine(argc, argv, &cmdline);
// Stop
    args_to_data(&edata, &cmdline);
    return edata;
}
