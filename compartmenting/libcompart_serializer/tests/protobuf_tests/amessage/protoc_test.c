// RUN: protoc-c --proto_path=%S --c_out=. amessage.proto 
// RUN: mv %S/amessage.pb-c.c %S/amessage.pb-c.cc
// RUN: %CC %C_FLAGS -I%S %s %S/amessage.pb-c.cc -lprotobuf-c -o %t
// RUN: %t | %FileCheck %s

#include <stdio.h>
#include <stdlib.h>
#include "amessage.pb-c.h"

int main (int argc, const char * argv[]) 
{
    AMessage msg = AMESSAGE__INIT;
    msg.a = 1;
    msg.has_b = 1;
    msg.b = 2;
    
    unsigned len = amessage__get_packed_size(&msg);

    void* buf = malloc(len);
    amessage__pack(&msg, buf);
    
    AMessage* msg2 = amessage__unpack(NULL, len, buf);
    
    printf("%d, %d, %d\n", msg2->a, msg2->has_b, msg2->b);
//CHECK: 1, 1, 2
    amessage__free_unpacked(msg2, NULL);

    free(buf); // Free the allocated serialized buffer
    return 0;
}
