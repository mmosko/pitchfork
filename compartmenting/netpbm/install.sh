#!/bin/sh
set -e

#sudo apt-get install libpng16-16 libpng16-dev
#sudo apt-get install libjpeg9-dev libjpeg9
#sudo apt-get install libtiff5-dev libtiff5

tar xzf netpbm-10.73.28.tgz
cp config.mk netpbm-10.73.28
make -C netpbm-10.73.28
cp netpbm-10.73.28/converter/other/tifftopnm tifftopnm_original
