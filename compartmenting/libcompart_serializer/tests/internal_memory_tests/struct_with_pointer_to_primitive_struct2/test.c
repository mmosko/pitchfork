// RUN: printf "TestPrimitiveStructPointer2\n\n" | python -B setup.py struct_interface.h
// RUN: %CC %C_FLAGS -I../headers %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG="%t.log" valgrind --leak-check=full %t 2>&1 || true; } | %FileCheck %s
// ADD TURN RN to RUN TO SEE RESULTS TO A FILE
// RN: sudo PITCHFORK_LOG="%t.log" valgrind --leak-check=full %t &> %t.valgrind

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "structs.h"
#include "compart_api.h"
#include "struct_interface.h"

int main()
{
	struct PrimitiveStructPointer2* primitive_struct = calloc(sizeof(*primitive_struct), 1);
	primitive_struct->hlv_pointer_1 = (struct StructWithPointerToPrimitiveStruct*) calloc(sizeof(struct StructWithPointerToPrimitiveStruct), 1);
	primitive_struct->hlv_pointer_1->primitive_struct = (struct PrimitiveStruct*) calloc(sizeof(struct PrimitiveStruct), 1);
	primitive_struct->hlv_pointer_2 = (struct StructWithPointerToPrimitiveStruct*) calloc(sizeof(struct StructWithPointerToPrimitiveStruct), 1);
	primitive_struct->hlv_pointer_2->primitive_struct = (struct PrimitiveStruct*) calloc(sizeof(struct PrimitiveStruct), 1);
	primitive_struct->string = malloc(strlen(INITIAL_STRING) + 1);
	strcpy(primitive_struct->string, INITIAL_STRING);

	primitive_struct->integer = INITIAL_NUMBER;
	primitive_struct->hlv_pointer_1->integer = INITIAL_NUMBER;
	primitive_struct->hlv_pointer_1->primitive_struct->integer = INITIAL_NUMBER;
	primitive_struct->hlv_pointer_1->primitive_struct->character = INITIAL_CHARACTER; 
	primitive_struct->hlv_pointer_2->integer = INITIAL_NUMBER;
	primitive_struct->hlv_pointer_2->primitive_struct->integer = INITIAL_NUMBER;
	primitive_struct->hlv_pointer_2->primitive_struct->character = INITIAL_CHARACTER; 

	struct extension_data arg = ext_TestPrimitiveStructPointer2_to_arg(primitive_struct);
	int result = ext_TestPrimitiveStructPointer2_from_resp(ext_TestPrimitiveStructPointer2(arg));
	if(result > 0)
	{
		printf("VULNERABLE, GOT A RESULT OF %d\n", result);
	}
	else
	{
		printf("NOT VULNERABLE\n");
// CHECK: NOT VULNERABLE
// CHECK-NOT: definitely lost
// CHECK-NOT: LEAK SUMMARY
	}

	fflush(stdout);
	free(primitive_struct->hlv_pointer_1->primitive_struct);
	free(primitive_struct->hlv_pointer_1);
	free(primitive_struct->hlv_pointer_2->primitive_struct);
	free(primitive_struct->hlv_pointer_2);
	free(primitive_struct->string);
	free(primitive_struct);
	return 0;
}
