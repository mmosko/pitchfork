// RUN: printf "TestStructWithString\n10\n" | python -B setup.py struct_interface.h
// RUN: %CC %C_FLAGS -I../headers %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "structs.h"
#include "compart_api.h"
#include "struct_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);

	struct extension_id* struct_ext = compart_register_fn("other compartment", &ext_TestStructWithString);

  	compart_start("struct compartment");

	struct StructWithString* struct_with_string = calloc(sizeof(*struct_with_string), 1);
	struct_with_string->integer = INITIAL_NUMBER;
	struct_with_string->string = malloc(strlen(INITIAL_STRING)+1);
	strcpy(struct_with_string->string, INITIAL_STRING);

	struct extension_data arg = ext_TestStructWithString_to_arg(struct_with_string);
	int result = ext_TestStructWithString_from_resp(compart_call_fn(struct_ext, arg));
	printf("result: %d", result);
	if(result > 0)
	{
		printf("VULNERABLE, GOT A RESULT OF %d\n", result);
	}
	else
	{
		printf("NOT VULNERABLE\n");
// CHECK: NOT VULNERABLE
	}

	fflush(stdout);
	free(struct_with_string);
	ext_TestStructWithString_from_resp(compart_call_fn(struct_ext, arg));
	return 0;
}
