
#include <string.h>
#include <stdlib.h>


void marshall_struct_StructWithString(char* buf, size_t* buf_index_, struct StructWithString* TEST_data);

void ext_TestStructWithString_from_arg(struct extension_data data, struct StructWithString **s);

struct extension_data ext_TestStructWithString(struct extension_data data);

int ext_TestStructWithString_from_resp(struct extension_data data);

void _unmarshall_struct_StructWithString(char* buf, size_t* buf_index_, struct StructWithString** TEST_data);

void clean_up(struct StructWithString *s);

struct extension_data ext_TestStructWithString_to_resp(int result);

struct extension_data ext_TestStructWithString_to_arg(struct StructWithString *s);

#define unmarshall_struct_StructWithString(a, b, c) 	_unmarshall_struct_StructWithString(a, b, &c)

#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
void marshall_struct_StructWithString(char* buf, size_t* buf_index_, struct StructWithString* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->integer);
    marshall_string(buf, &buf_index, TEST_data->string);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_StructWithString(char* buf, size_t* buf_index_, struct StructWithString** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct StructWithString* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->integer);
    unmarshall_string(buf, &buf_index, TEST_data_ref->string);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_StructWithString(a, b, c) 	_unmarshall_struct_StructWithString(a, b, &c)

void clean_up(struct StructWithString *s)
{
  free(s->string);
  if (s != NULL) free(s);
}
void ext_TestStructWithString_from_arg(struct extension_data data, struct StructWithString **s)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_struct_StructWithString(buf, &buf_index, *s);
}
struct extension_data ext_TestStructWithString(struct extension_data data)
{
  struct StructWithString *s;
  ext_TestStructWithString_from_arg(data, &s);


//TODO fix, this is bug with libcompart not shutting down with return value of 0

if(s->integer == INITIAL_NUMBER && s->string != NULL && strcmp(s->string, INITIAL_STRING) == 0) {
	s->integer = SPECIAL_NUMBER_1;
	strcpy(s->string, SPECIAL_STRING);
}

  int return_value;
  return_value = TestStructWithString(s);
  struct extension_data result = ext_TestStructWithString_to_resp( return_value );
  clean_up(s);
  return result;
}
int ext_TestStructWithString_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
struct extension_data ext_TestStructWithString_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
struct extension_data ext_TestStructWithString_to_arg(struct StructWithString *s)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_struct_StructWithString(buf, &buf_index, s);
  data.bufc = buf_index;
  return data;
}
