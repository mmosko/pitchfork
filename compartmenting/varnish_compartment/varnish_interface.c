#include "varnish_interface.h"
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "signal.h"
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/wait.h>

struct compart comparts[NO_COMPARTS] = {
  {.name = "varnish", .uid = 65534, .gid = 65534, .path = "/var/empty/varnish"},
  {.name = "priv", .uid = 0, .gid = 0, .path = "/var/empty/priv"}
};
struct extension_id * memcpy_ext = NULL;

/*****************************************
 * MARK: Code for marshalling/unmarshalling
 * (All copied from wget-2017 patch interface.c)
 * ******/

#define MARSHALL_CAT_(a, b) \
        a##b \

#define MARSHALL_CAT(a, b) \
        MARSHALL_CAT_(a, b) \

void marshall_string(char* buf, size_t* buf_index_, char* str) {
  size_t buf_index = *buf_index_;
  if (str) {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  } else {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }
  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str) {
  *str = NULL;
  size_t buf_index = *buf_index_;
  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);
  if (str_length > 0) {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }
  *buf_index_ = buf_index;
}

#define unmarshall_string(buf, buf_index, str) \
        unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); \
  memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__); \

#define unmarshall_prim(buf, buf_index, data) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); \
  memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__); \


/**************************
 * MARK: Interface used for cache_fetch
 * ******/ 

/* This runs on priv, patching memcpy(dest, src, len) */
struct extension_data ext_memcpy(struct extension_data data) {
  struct extension_data result;
  void* dst;
  char* src;
  ssize_t l;
  
  char* buf = data.buf;
  size_t buf_index = 0;
  
  unmarshall_prim(buf, &buf_index, dst);
  unmarshall_string(buf, &buf_index, src);
  unmarshall_prim(buf, &buf_index, l);
  
  // get real length in priv
  char* priv_dest = (char*) malloc(l);
  memcpy(priv_dest, src, l);
  size_t priv_len = strlen(priv_dest);
  
  char* result_buf = result.buf;
  size_t result_buf_index = 0;
  marshall_prim(result_buf, &result_buf_index, dst);
  marshall_string(result_buf, &result_buf_index, priv_dest);
  marshall_prim(result_buf, &result_buf_index, priv_len);
  result.bufc = result_buf_index;
  return result;
}

/* This is called by compartment "varnish" */
struct extension_data ext_memcpy_to_arg(void * dst, char* src, ssize_t l) {
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  
  marshall_prim(buf, &buf_index, dst);
  marshall_string(buf, &buf_index, src);
  marshall_prim(buf, &buf_index, l);
  
  data.bufc = buf_index;
  return data;
}

/* This does the memcpy in compartment "varnish" */
void ext_memcpy_from_resp(struct extension_data data) {
  void* dst;
  char* src;
  size_t l;
  
  char* buf = data.buf;
  size_t buf_index = 0;
  
  unmarshall_prim(buf, &buf_index, dst);
  unmarshall_string(buf, &buf_index, src);
  unmarshall_prim(buf, &buf_index, l);
  
  memcpy((char*)dst, src, l);
}