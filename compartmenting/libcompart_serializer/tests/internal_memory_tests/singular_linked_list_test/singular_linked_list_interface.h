
#include <string.h>
#include <stdlib.h>


void _unmarshall_struct_Node(char* buf, size_t* buf_index_, struct Node** TEST_data);

void ll_cleaner(struct Node *head);

void clean_up(struct Node *head);

struct extension_data ext_TestLinkedList(struct extension_data data);

struct extension_data ext_TestLinkedList_to_resp(int result);

struct extension_data ext_TestLinkedList_to_arg(struct Node *head);

void marshall_struct_Node(char* buf, size_t* buf_index_, struct Node* TEST_data);

int ext_TestLinkedList_from_resp(struct extension_data data);

void ext_TestLinkedList_from_arg(struct extension_data data, struct Node **head);

#define unmarshall_struct_Node(a, b, c) 	_unmarshall_struct_Node(a, b, &c)

#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
void _unmarshall_struct_Node(char* buf, size_t* buf_index_, struct Node** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct Node* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->val);
    struct Node* node = TEST_data_ref; 
    size_t linked_list_length = 0;
    unmarshall_prim(buf, &buf_index, linked_list_length); 
    for(size_t i = 0; i < linked_list_length; i++) 
    { 
    	struct Node* n_node; 
    	unmarshall_size(buf, &buf_index, n_node, 1); 
    	node->next = n_node; 
    	n_node->next = NULL; 
    	node = n_node; 
    } 
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_Node(a, b, c) 	_unmarshall_struct_Node(a, b, &c)

void marshall_struct_Node(char* buf, size_t* buf_index_, struct Node* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->val);
    struct Node* node = TEST_data->next; 
    size_t linked_list_length = 0;
    struct Node* counter_node = node; 
    while(counter_node != NULL) 
    { 
    	counter_node = counter_node->next; 
    	linked_list_length++; 
    } 
    marshall_prim(buf, &buf_index, linked_list_length); 
    while(node != NULL) 
    { 
    	marshall_size(buf, &buf_index, node, 1); 
    	node = node->next; 
    } 
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void ll_cleaner(struct Node *head)
{
  struct Node *head_ref = head;
  while (head_ref != NULL) {
    head = head->next;
    free(head_ref);
    head_ref = NULL;
    head_ref = head;
  }
}
void clean_up(struct Node *head)
{
  ll_cleaner(head);
}
struct extension_data ext_TestLinkedList(struct extension_data data)
{
  struct Node *head;
  ext_TestLinkedList_from_arg(data, &head);



  int return_value;
  return_value = TestLinkedList(head);
  struct extension_data result = ext_TestLinkedList_to_resp( return_value );
  clean_up(head);
  return result;
}
struct extension_data ext_TestLinkedList_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
struct extension_data ext_TestLinkedList_to_arg(struct Node *head)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_struct_Node(buf, &buf_index, head);
  data.bufc = buf_index;
  return data;
}
int ext_TestLinkedList_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
void ext_TestLinkedList_from_arg(struct extension_data data, struct Node **head)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_struct_Node(buf, &buf_index, *head);
}
