# Installation

This will install new software onto the system. Check `setup.sh` for more details 

`sudo ./setup.sh`

#Expected output
```
henry@henry-vm:~/chopchop/compartmenting/libcompart_serializer/tests/protobuf_tests$ lit . -v
lit: /home/henry/chopchop/compartmenting/libcompart_serializer/tests/lit.site.cfg:74: note: Repository root is /home/henry/chopchop/compartmenting
lit: /home/henry/chopchop/compartmenting/libcompart_serializer/tests/lit.site.cfg:83: note: Using build type Debug
rm -f compart.o libcompart.so compost.o compart_general.o
cc -Wall -O0 -g -DINCLUDE_PID -I/home/henry/chopchop/compartmenting/libcompart  -DPITCHFORK_DBGSTDOUT -DINCLUDE_PID  -c compost.c
PF_FLAGS=-DPITCHFORK_DBGSTDOUT -DINCLUDE_PID
cc -Wall -O0 -g -DINCLUDE_PID -I/home/henry/chopchop/compartmenting/libcompart  -DPITCHFORK_DBGSTDOUT -DINCLUDE_PID  -c compart.c -o compart_general.o
ld -relocatable compart_general.o compost.o -o compart.o
-- Testing: 4 tests, single process --
PASS: Testing libcompart_serializer :: protobuf_tests/amessage/protoc_test.c (1 of 4)
PASS: Testing libcompart_serializer :: protobuf_tests/amessage/test.c (2 of 4)
PASS: Testing libcompart_serializer :: protobuf_tests/dmessage/protoc_test.c (3 of 4)
PASS: Testing libcompart_serializer :: protobuf_tests/dmessage/test.c (4 of 4)
Testing Time: 0.47s
  Expected Passes    : 4
```
