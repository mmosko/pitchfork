#!/bin/bash

wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/redis/redis-2.0.2.tar.gz
tar xvf redis-2.0.2.tar.gz
cd ocaml-4.01.0
./configure
make world.opt
sudo make install

git clone https://github.com/plum-umd/c-strider
make

