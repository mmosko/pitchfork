#pragma once

#include <stdlib.h>
#include <string.h>

#define TEST_SUCCESS 0
#define TEST_FAILURE 1

#define INITIAL_NUMBER 2
#define INITIAL_CHARACTER 3
#define INITIAL_STRING "Vulnerable"

#define SPECIAL_NUMBER_1 1729
#define SPECIAL_CHARACTER_1 36
#define SPECIAL_STRING "Safe"

struct Node
{
    int val;
    struct Node *next;
};
struct D_Node
{
    int val;
    struct D_Node *next;
    struct D_Node *prev;
};
struct PrimitiveStruct
{
    int integer;
    char character;
};

typedef struct
{
    int integer;
    char character;
} TypedefPrimitiveStruct;

typedef struct PrimitiveStruct2
{
    int integer;
    char character;
} TypedefPrimitiveStruct2;

struct StructWithString
{
    int integer;
    char *string;
};

struct StructWithPrimitiveStruct
{
    int integer;
    struct PrimitiveStruct primitive_struct;
};

struct StructWithPointerToPrimitiveStruct
{
    int integer;
    struct PrimitiveStruct *primitive_struct;
};

struct PrimitiveStructPointer2
{
    int integer;
    char *string;
    struct StructWithPointerToPrimitiveStruct *hlv_pointer_1;
    struct StructWithPointerToPrimitiveStruct *hlv_pointer_2;
};

#define CAT_(a, b) a##b
#define CAT(a, b) CAT_(a, b)

int TestPrimitiveStruct(struct PrimitiveStruct *s)
{
    if (s->integer == SPECIAL_NUMBER_1 && s->character == SPECIAL_CHARACTER_1)
    {
        return TEST_SUCCESS;
    }

    return TEST_FAILURE;
}

int TestTypedefPrimitiveStruct(TypedefPrimitiveStruct *s)
{
    if (s->integer == SPECIAL_NUMBER_1 && s->character == SPECIAL_CHARACTER_1)
    {
        return TEST_SUCCESS;
    }

    return TEST_FAILURE;
}

//TODO as done above, check for SUCCESS instead of FAILURE to better test marshalling/demarshalling
int TestPrimitiveStruct2(struct PrimitiveStruct2 *s)
{
    if (s == NULL)
    {
        return TEST_FAILURE;
    }

    if (s->integer == 0 || s->character == 0)
    {
        return TEST_FAILURE;
    }

    return TEST_SUCCESS;
}

int TestTypedefPrimitiveStruct2(TypedefPrimitiveStruct2 *s)
{
    if (s == NULL)
    {
        return TEST_FAILURE;
    }

    if (s->integer == SPECIAL_NUMBER_1 || s->character == SPECIAL_CHARACTER_1)
    {
        return TEST_SUCCESS;
    }

    return TEST_FAILURE;
}

int TestStructWithString(struct StructWithString *s)
{
    if (s == NULL)
    {
        return TEST_FAILURE;
    }

    if (s->integer == SPECIAL_NUMBER_1 && s->string != NULL && strcmp(s->string, SPECIAL_STRING) == 0)
    {
        return TEST_SUCCESS;
    }

    return TEST_FAILURE;
}

int TestStructWithPrimitiveStruct(struct StructWithPrimitiveStruct *s)
{
    if (s == NULL)
    {
        return TEST_FAILURE;
    }

    if (s->integer == SPECIAL_NUMBER_1 && s->primitive_struct.integer == SPECIAL_NUMBER_1 && s->primitive_struct.character == SPECIAL_CHARACTER_1)
    {
        return TEST_SUCCESS;
    }

    return TEST_FAILURE;
}

int TestStructWithPointerToPrimitiveStruct(struct StructWithPointerToPrimitiveStruct *s)
{
    if (s == NULL)
    {
        return TEST_FAILURE;
    }

    if (s->integer == SPECIAL_NUMBER_1 && s->primitive_struct->integer == SPECIAL_NUMBER_1 && s->primitive_struct->character == SPECIAL_CHARACTER_1)
    {
        return TEST_SUCCESS;
    }

    return TEST_FAILURE;
}

int TestPrimitiveStructPointer2(struct PrimitiveStructPointer2 *s)
{
    if (s == NULL)
    {
        return TEST_FAILURE;
    }

    if (s->integer == SPECIAL_NUMBER_1 && strcmp(s->string, SPECIAL_STRING) == 0 && s->hlv_pointer_1->integer == SPECIAL_NUMBER_1 && s->hlv_pointer_1->primitive_struct->integer == SPECIAL_NUMBER_1 && s->hlv_pointer_1->primitive_struct->character == SPECIAL_CHARACTER_1 && s->hlv_pointer_2->integer == SPECIAL_NUMBER_1 && s->hlv_pointer_2->primitive_struct->integer == SPECIAL_NUMBER_1 && s->hlv_pointer_2->primitive_struct->character == SPECIAL_CHARACTER_1)
    {
        return TEST_SUCCESS;
    }

    return TEST_FAILURE;
}

int TestLinkedList(struct Node *head)
{
    struct Node *tmp = head;
    if (tmp->val != 1)
        return 1;

    tmp = tmp->next;

    if (tmp->val != 2)
        return 1;

    tmp = tmp->next;

    if (tmp->val != 3)
        return 1;

    return 0;
}

int TestDoublyLinkedList(struct D_Node *head)
{
    struct Node *tmp = head;
    if (tmp->val != 1)
        return 1;

    tmp = tmp->next;

    if (tmp->val != 2)
        return 1;

    tmp = tmp->next;

    if (tmp->val != 3)
        return 1;

    return 0;
}

