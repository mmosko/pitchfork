#pragma once

#include "db_info.h"

#define DB_INFO_NEXT NULL
#define DB_INFO_P 'c'

#define SUCCESS 1
#define NO_SUCCESS 0

int return_same(dbinfo* db)
{
    if(db->next == DB_INFO_NEXT && db->p == DB_INFO_P)
    {
        return SUCCESS;
    }
	return NO_SUCCESS;
}

