
#include <string.h>
#include <stdlib.h>

#include "amessage.pb-c.h"
#include "return_same.h"

#define NO_COMPARTS 2

static struct compart comparts[NO_COMPARTS] =
  {{.name = "hello compartment", .uid = 65534, .gid = 65534, .path = "/dev/"},
   {.name = "other compartment", .uid = 65534, .gid = 65534, .path = "/dev/"}};

struct extension_id *return_same_ext = NULL;

struct extension_data ext_return_same_to_resp(int result);

void marshall_struct_ProtobufCMessageDescriptor(char* buf, size_t* buf_index_, struct ProtobufCMessageDescriptor* TEST_data);

void _unmarshall_struct__AMessage(char* buf, size_t* buf_index_, struct _AMessage** TEST_data);

void marshall_struct_ProtobufCFieldDescriptor(char* buf, size_t* buf_index_, struct ProtobufCFieldDescriptor* TEST_data);

struct extension_data ext_return_same(struct extension_data data);

void _unmarshall_struct_ProtobufCMessageDescriptor(char* buf, size_t* buf_index_, struct ProtobufCMessageDescriptor** TEST_data);

int ext_return_same_from_resp(struct extension_data data);

void marshall_struct__AMessage(char* buf, size_t* buf_index_, struct _AMessage* TEST_data);

void _unmarshall_struct_ProtobufCIntRange(char* buf, size_t* buf_index_, struct ProtobufCIntRange** TEST_data);

void marshall_struct_ProtobufCMessageUnknownField(char* buf, size_t* buf_index_, struct ProtobufCMessageUnknownField* TEST_data);

void _unmarshall_struct_ProtobufCFieldDescriptor(char* buf, size_t* buf_index_, struct ProtobufCFieldDescriptor** TEST_data);

void _unmarshall_struct_ProtobufCMessage(char* buf, size_t* buf_index_, struct ProtobufCMessage** TEST_data);

struct extension_data ext_return_same_to_arg(struct _AMessage* amessage);

void ext_return_same_from_arg(struct extension_data data, struct _AMessage* *amessage);

void _unmarshall_struct_ProtobufCMessageUnknownField(char* buf, size_t* buf_index_, struct ProtobufCMessageUnknownField** TEST_data);

void marshall_struct_ProtobufCIntRange(char* buf, size_t* buf_index_, struct ProtobufCIntRange* TEST_data);

void marshall_struct_ProtobufCMessage(char* buf, size_t* buf_index_, struct ProtobufCMessage* TEST_data);

#define unmarshall_struct__AMessage(a, b, c) 	_unmarshall_struct__AMessage(a, b, &c)

#define unmarshall_struct_ProtobufCMessageDescriptor(a, b, c) 	_unmarshall_struct_ProtobufCMessageDescriptor(a, b, &c)

#define unmarshall_struct_ProtobufCIntRange(a, b, c) 	_unmarshall_struct_ProtobufCIntRange(a, b, &c)

#define unmarshall_struct_ProtobufCFieldDescriptor(a, b, c) 	_unmarshall_struct_ProtobufCFieldDescriptor(a, b, &c)

#define unmarshall_struct_ProtobufCMessage(a, b, c) 	_unmarshall_struct_ProtobufCMessage(a, b, &c)

#define unmarshall_struct_ProtobufCMessageUnknownField(a, b, c) 	_unmarshall_struct_ProtobufCMessageUnknownField(a, b, &c)

#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
void marshall_struct_ProtobufCMessageDescriptor(char* buf, size_t* buf_index_, struct ProtobufCMessageDescriptor* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->message_init);
    marshall_prim(buf, &buf_index, TEST_data->magic);
    marshall_prim(buf, &buf_index, TEST_data->name);
    marshall_prim(buf, &buf_index, TEST_data->package_name);
    marshall_struct_ProtobufCFieldDescriptor(buf, &buf_index, TEST_data->fields);
    marshall_prim(buf, &buf_index, TEST_data->fields_sorted_by_name);
    marshall_prim(buf, &buf_index, TEST_data->short_name);
    marshall_prim(buf, &buf_index, TEST_data->sizeof_message);
    marshall_struct_ProtobufCIntRange(buf, &buf_index, TEST_data->field_ranges);
    marshall_prim(buf, &buf_index, TEST_data->n_fields);
    marshall_prim(buf, &buf_index, TEST_data->reserved2);
    marshall_prim(buf, &buf_index, TEST_data->reserved1);
    marshall_prim(buf, &buf_index, TEST_data->n_field_ranges);
    marshall_prim(buf, &buf_index, TEST_data->reserved3);
    marshall_prim(buf, &buf_index, TEST_data->c_name);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct__AMessage(char* buf, size_t* buf_index_, struct _AMessage** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct _AMessage* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->a);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->has_b);
    {
      int32_t* tmp = NULL;
      unmarshall_struct_ProtobufCMessage(buf, &buf_index, tmp);
      memcpy(&TEST_data_ref->base, tmp, sizeof(*tmp));
      free(tmp);
    }
    unmarshall_prim(buf, &buf_index, TEST_data_ref->b);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct__AMessage(a, b, c) 	_unmarshall_struct__AMessage(a, b, &c)

void marshall_struct_ProtobufCFieldDescriptor(char* buf, size_t* buf_index_, struct ProtobufCFieldDescriptor* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->default_value);
    marshall_prim(buf, &buf_index, TEST_data->name);
    marshall_prim(buf, &buf_index, TEST_data->reserved_flags);
    marshall_prim(buf, &buf_index, TEST_data->offset);
    marshall_prim(buf, &buf_index, TEST_data->label);
    marshall_prim(buf, &buf_index, TEST_data->descriptor);
    marshall_prim(buf, &buf_index, TEST_data->flags);
    marshall_prim(buf, &buf_index, TEST_data->quantifier_offset);
    marshall_prim(buf, &buf_index, TEST_data->type);
    marshall_prim(buf, &buf_index, TEST_data->id);
    marshall_prim(buf, &buf_index, TEST_data->reserved3);
    marshall_prim(buf, &buf_index, TEST_data->reserved2);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_ProtobufCMessageDescriptor(char* buf, size_t* buf_index_, struct ProtobufCMessageDescriptor** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct ProtobufCMessageDescriptor* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->message_init);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->magic);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->name);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->package_name);
    unmarshall_struct_ProtobufCFieldDescriptor(buf, &buf_index, TEST_data_ref->fields);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->fields_sorted_by_name);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->short_name);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->sizeof_message);
    unmarshall_struct_ProtobufCIntRange(buf, &buf_index, TEST_data_ref->field_ranges);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->n_fields);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->reserved2);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->reserved1);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->n_field_ranges);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->reserved3);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->c_name);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_ProtobufCMessageDescriptor(a, b, c) 	_unmarshall_struct_ProtobufCMessageDescriptor(a, b, &c)

void marshall_struct__AMessage(char* buf, size_t* buf_index_, struct _AMessage* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->a);
    marshall_prim(buf, &buf_index, TEST_data->has_b);
    marshall_struct_ProtobufCMessage(buf, &buf_index, &TEST_data->base);
    marshall_prim(buf, &buf_index, TEST_data->b);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_ProtobufCIntRange(char* buf, size_t* buf_index_, struct ProtobufCIntRange** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct ProtobufCIntRange* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->orig_index);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->start_value);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_ProtobufCIntRange(a, b, c) 	_unmarshall_struct_ProtobufCIntRange(a, b, &c)

void marshall_struct_ProtobufCMessageUnknownField(char* buf, size_t* buf_index_, struct ProtobufCMessageUnknownField* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->wire_type);
    marshall_prim(buf, &buf_index, TEST_data->tag);
    marshall_prim(buf, &buf_index, TEST_data->data);
    marshall_prim(buf, &buf_index, TEST_data->len);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_ProtobufCFieldDescriptor(char* buf, size_t* buf_index_, struct ProtobufCFieldDescriptor** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct ProtobufCFieldDescriptor* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->default_value);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->name);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->reserved_flags);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->offset);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->label);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->descriptor);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->flags);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->quantifier_offset);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->type);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->id);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->reserved3);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->reserved2);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_ProtobufCFieldDescriptor(a, b, c) 	_unmarshall_struct_ProtobufCFieldDescriptor(a, b, &c)

void _unmarshall_struct_ProtobufCMessage(char* buf, size_t* buf_index_, struct ProtobufCMessage** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct ProtobufCMessage* TEST_data_ref = *TEST_data;
    unmarshall_struct_ProtobufCMessageDescriptor(buf, &buf_index, TEST_data_ref->descriptor);
    unmarshall_struct_ProtobufCMessageUnknownField(buf, &buf_index, TEST_data_ref->unknown_fields);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->n_unknown_fields);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_ProtobufCMessage(a, b, c) 	_unmarshall_struct_ProtobufCMessage(a, b, &c)

void _unmarshall_struct_ProtobufCMessageUnknownField(char* buf, size_t* buf_index_, struct ProtobufCMessageUnknownField** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct ProtobufCMessageUnknownField* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->wire_type);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->tag);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->data);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->len);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_ProtobufCMessageUnknownField(a, b, c) 	_unmarshall_struct_ProtobufCMessageUnknownField(a, b, &c)

void marshall_struct_ProtobufCIntRange(char* buf, size_t* buf_index_, struct ProtobufCIntRange* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->orig_index);
    marshall_prim(buf, &buf_index, TEST_data->start_value);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void marshall_struct_ProtobufCMessage(char* buf, size_t* buf_index_, struct ProtobufCMessage* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_struct_ProtobufCMessageDescriptor(buf, &buf_index, TEST_data->descriptor);
    marshall_struct_ProtobufCMessageUnknownField(buf, &buf_index, TEST_data->unknown_fields);
    marshall_prim(buf, &buf_index, TEST_data->n_unknown_fields);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
struct extension_data ext_return_same_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
struct extension_data ext_return_same(struct extension_data data)
{
  struct _AMessage* amessage;
  ext_return_same_from_arg(data, &amessage);
  int return_value;
  return_value = return_same(amessage);
  struct extension_data result = ext_return_same_to_resp( return_value );
  return result;
}
int ext_return_same_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
struct extension_data ext_return_same_to_arg(struct _AMessage* amessage)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_struct__AMessage(buf, &buf_index, amessage);
  data.bufc = buf_index;
  return data;
}
void ext_return_same_from_arg(struct extension_data data, struct _AMessage* *amessage)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_struct__AMessage(buf, &buf_index, *amessage);
}
