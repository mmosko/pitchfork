#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "toy.h"

void initkey() {
  for (int i = 0; i < 50; i++) {
    key[i] = 'a';
  }
}

void greeter (char *str) {
  printf(str); printf(", welcome!\n");
}

void encrypt (char * plaintext, int sz) {
  ciphertext = (char * ) (malloc (sz));
  for (int i=0; i<sz; i++) {
    ciphertext[i]=plaintext[i] ^ key[i];
  }
}

int main (int argc, char **argv) {
  char username[64], text[1024];
  char *key_ptr = key;

  initkey();

  printf("Enter username: ");
  fgets(username, sizeof(username), stdin);
  greeter(username);

  printf("Enter plaintext: ");
  fgets(text, sizeof(text), stdin);

  encrypt(text, strlen(text));

  printf("Cipher text: ");
  for (unsigned i=0; i<strlen(text); i++) {
    printf("%x ",ciphertext[i]);
  }
  printf("\n");
  return 0;
}
