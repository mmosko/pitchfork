#pragma once

#include <stdlib.h>

struct Node {
	struct Node* left;
	struct Node* right;
	int val;
};

struct Node* AllocateNode(int val) {
	struct Node* node = calloc(sizeof(struct Node), 1);
	node->left = NULL;
	node->right = NULL;
	node->val = val;

	return node;
}

struct Node* AppendNodeToLeft(struct Node* parent, int val) {
	struct Node* new_leaf = AllocateNode(val);

	parent->left = new_leaf;
	return new_leaf;
}

struct Node* AppendNodeToRight(struct Node* parent, int val) {
	struct Node* new_leaf = AllocateNode(val);

	parent->right = new_leaf;
	return new_leaf;
}

void FreeBinaryTree(struct Node* leaf) {
	if(leaf->left != NULL) {
		FreeBinaryTree(leaf->left);
	}
	if(leaf->right != NULL) {
		FreeBinaryTree(leaf->right);
	}
	free(leaf);
}

/*
     1
    / \
   2   3
  / \ / \
 4  N N  5
*/
int IsValidOriginalBinaryTree(struct Node* head) {
	if(head == NULL || head->val != 1) {
		return 0;
	}

	if(head->left == NULL || head->left->val != 2) {
		return 0;
	}

	if(head->right == NULL || head->right->val != 3) {
		return 0;
	}

	if(head->left->left == NULL || head->left->left->val != 4) {
		return 0;
	}
	
	if(head->right->right == NULL || head->right->right->val != 5) {
		return 0;
	}

	return 1;
}

/*
     5
    / \
   4   3
  / \ / \
 2  N N  1
*/
int IsInvalidBinaryTree(struct Node* head) {
	if(head == NULL || head->val != 5) {
        printf("1 %d\n", head->val);
		return 1;
	}

	if(head->left == NULL || head->left->val != 4) {
        //printf("2 %d\n", head->left->val);
        printf("2\n");
		return 1;
	}

	if(head->right == NULL || head->right->val != 3) {
        printf("3\n");
		return 1;
	}

	if(head->left->left == NULL || head->left->left->val != 2) {
        printf("4\n");
		return 1;
	}
	
	if(head->right->right == NULL || head->right->right->val != 1) {
        printf("5\n");
		return 1;
	}

	return 0;
}

struct Node* MakeValidBinaryTree() {
	struct Node* head = AllocateNode(5);
	struct Node* head_left = AppendNodeToLeft(head, 4);
	struct Node* head_right = AppendNodeToRight(head, 3);
	AppendNodeToLeft(head_left, 2);
	AppendNodeToRight(head_right, 1);
	return head;
}
