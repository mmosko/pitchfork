/*
   Copyright 2021 University of Pennsylvania
   Pitchfork project. Nik Sultana, UPenn, 2019

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#pragma once

#include "clang/AST/AST.h"
#include "clang/AST/Stmt.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Basic/TargetOptions.h"

namespace pitchfork {
using namespace clang;

class AnalysisCtxt {
 public:
  enum class TurnKind { None, Fun, IfThenElse, While, For, Block, Do };
  using Turn = const Stmt;
  using RemainingStmts = std::list<const Stmt *>;

  bool trace = false;
  // Leave the found statement for re-visiting when reachable_stmt is called again
  bool resume_include_found = true;

  struct Ctxt {
    Turn *turn;
    RemainingStmts *rs;
    bool found;
    TurnKind tk;
    bool do_trace = false;
  };

  struct Stack {
    std::list<Ctxt *> *frames;
    std::list<const Stmt *> *trace;
    bool do_trace = false;
  };

  using PCtxt = std::list<Stack *>;

 private:
  PCtxt *me;
  bool initial = true;

 public:
  std::list<Stack *>* stacks() {
    return me;
  }

  friend unsigned reachable_stmt(clang::LangOptions *lang_opt, SourceManager *SM, const Stmt *Target,
                                 AnalysisCtxt &stacks, std::list<const Stmt *> *external_trace,
                                 bool (*hook)(clang::LangOptions *lang_opt, clang::SourceManager *_SM, const Stmt *sought_stmt, const Stmt *stmt), bool descend_scopes);


  static std::string TurnKindString(TurnKind tk);

  explicit AnalysisCtxt() = delete;

  AnalysisCtxt(PCtxt *m) : me(m) {}

  AnalysisCtxt(const Stmt *s);

  Stack *clone(Stack *s);

  AnalysisCtxt(AnalysisCtxt &orig);

  static std::list<const Stmt *> *trace_statements(Stack *s);

  static std::string trace_statements_str(SourceManager *SM, Stack *s);

  void join(AnalysisCtxt *ac);

  unsigned filter_subsumed(bool strict);

  std::string trace_statements_str(SourceManager *SM);

  std::string stats();

  unsigned num_stacks();

  unsigned num_found();

  float found_ratio();

  bool stable();

  std::list<const Stmt *> *found_statements();

  std::string stats(SourceManager *SM);

  bool empty();

  void reset();

  static Ctxt *NewLayer(const Stmt *point, const Stmt *s, TurnKind tk,
                        bool b = false);

  static Ctxt *NewLayer(const Stmt *point, RemainingStmts *stmts, TurnKind tk,
                        bool b = false);

  PCtxt *filter();

  static bool equal(Turn *x, Turn *y);

  template <class T>
  static bool equalist(T *x, T *y);

  static bool equal(RemainingStmts *x, RemainingStmts *y);

  static bool equal(Stack *x, Stack *y, bool strict = false);

  static bool equal(Ctxt *x, Ctxt *y);

  unsigned deduplicate(bool non_strict_stack_factoring = false);

  bool subsumes(Ctxt *ctxt1, Ctxt *ctxt2);

  template <class T>
  bool subsumesShallow(std::list<T> *list1, std::list<T> *list2);

  template <class T>
  bool subsumesDeep(std::list<T> *list1, std::list<T> *list2);

  bool subsumes(const Stack *stack1, const Stack *stack2, bool strict = false);
};

unsigned reachable_stmt(clang::LangOptions *lang_opt, SourceManager *SM, const Stmt *Target,
                        AnalysisCtxt &stacks, std::list<const Stmt *> *external_trace = nullptr,
                        bool (*hook)(clang::LangOptions *lang_opt, clang::SourceManager *_SM, const Stmt *sought_stmt, const Stmt *stmt) = nullptr,
                        bool descend_scopes = true);
}  // namespace pitchfork
