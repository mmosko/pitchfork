import sys

sys.path.insert(0, '../../../')

import function_serializer

output_file = sys.argv[1]

def InsertLines(lines, start, new_lines):
	new_lines = new_lines.split('\n')
	for i, new_line in enumerate(new_lines):
		lines.insert(start + i, new_lines[i])

interface_header_generated_code, interface_generated_code = function_serializer.RunSerializer(['../headers/structs.h'], {'exclude_to_resp' : False, 'exclude_from_resp': False, 'exclude_ext_name': False, 'to_arg_name': None, 'from_arg_name': None, 'include_boilerplate': True})

interface_libcompart_code = '''
#include <string.h>
#include <stdlib.h>

'''
#add the libcompart code
interface = interface_libcompart_code + interface_header_generated_code + interface_generated_code

lines = interface.split('\n')

for idx, line in enumerate(lines):
	#update the code that the parent compartment checks:
	#struct extension_data ext_add_ten(struct extension_data data)
	#make a if the value is greater than ten and patch it
	if 'from_arg(data, ' in line:
		idx = idx + 1
		new_lines = '''

//TODO fix, this is bug with libcompart not shutting down with return value of 0

if(s->integer == INITIAL_NUMBER && s->primitive_struct->integer == INITIAL_NUMBER && s->primitive_struct->character == INITIAL_CHARACTER ) {
	s->integer = SPECIAL_NUMBER_1;
	s->primitive_struct->integer = SPECIAL_NUMBER_1;
	s->primitive_struct->character = SPECIAL_CHARACTER_1;
}
'''
		InsertLines(lines, idx, new_lines)
		break

interface_code = '\n'.join(lines)

#write the interface code

with open(output_file, 'w') as f:
	f.write(interface_code)



