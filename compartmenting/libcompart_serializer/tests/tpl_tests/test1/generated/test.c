// RUN: printf "return_same\n1\n" | python -B setup.py tpl_interface.h
// RUN: %CC %C_FLAGS -I../add_function %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "compart_api.h"
#include "return_same.h"
#include "tpl_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);
  	return_same_ext = compart_register_fn("other compartment", &ext_return_same);

  	compart_start("hello compartment");

	int i,j = -1;

    i = 1;
	struct extension_data arg = ext_return_same_to_arg(&i);
	j = ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));

	printf("i: %d, j: %d\n", i, j);
// CHECK: i: 1, j: 1

	fflush(stdout);
	ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));
	return 0;
}

