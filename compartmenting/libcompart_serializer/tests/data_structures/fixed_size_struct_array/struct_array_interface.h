
#include "header.h"
#include <string.h>
#include <stdlib.h>

#define NO_COMPARTS 2

static struct compart comparts[NO_COMPARTS] =
  {{.name = "struct compartment", .uid = 65534, .gid = 65534, .path = "/dev/"},
   {.name = "other compartment", .uid = 65534, .gid = 65534, .path = "/dev/"}};


void ext_test_state_from_arg(struct extension_data data, struct State **s);

struct extension_data ext_test_state_to_resp(int result);

void marshall_struct_URLGlob(char* buf, size_t* buf_index_, struct URLGlob* TEST_data);

int ext_test_state_from_resp(struct extension_data data);

void marshall_struct_State(char* buf, size_t* buf_index_, struct State* TEST_data);

struct extension_data ext_test_state(struct extension_data data);

void _unmarshall_struct_URLGlob(char* buf, size_t* buf_index_, struct URLGlob** TEST_data);

struct extension_data ext_test_state_to_arg(struct State *s);

void _unmarshall_struct_State(char* buf, size_t* buf_index_, struct State** TEST_data);

#define unmarshall_struct_URLGlob(a, b, c) 	_unmarshall_struct_URLGlob(a, b, &c)

#define unmarshall_struct_State(a, b, c) 	_unmarshall_struct_State(a, b, &c)

#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
void marshall_struct_URLGlob(char* buf, size_t* buf_index_, struct URLGlob* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->pattern);
    marshall_prim(buf, &buf_index, TEST_data->pos);
    marshall_prim(buf, &buf_index, TEST_data->urllen);
    marshall_prim(buf, &buf_index, TEST_data->error);
    marshall_prim(buf, &buf_index, TEST_data->beenhere);
    marshall_string(buf, &buf_index, TEST_data->glob_buffer);
    marshall_prim(buf, &buf_index, TEST_data->size);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void marshall_struct_State(char* buf, size_t* buf_index_, struct State* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->urlnum);
    marshall_string(buf, &buf_index, TEST_data->uploadfile);
    marshall_struct_URLGlob(buf, &buf_index, TEST_data->inglob);
    marshall_prim(buf, &buf_index, TEST_data->up);
    marshall_struct_URLGlob(buf, &buf_index, TEST_data->urls);
    marshall_string(buf, &buf_index, TEST_data->outfiles);
    marshall_prim(buf, &buf_index, TEST_data->li);
    marshall_prim(buf, &buf_index, TEST_data->infilenum);
    marshall_string(buf, &buf_index, TEST_data->httpgetfields);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_struct_URLGlob(char* buf, size_t* buf_index_, struct URLGlob** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct URLGlob* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->pattern);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->pos);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->urllen);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->error);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->beenhere);
    unmarshall_string(buf, &buf_index, TEST_data_ref->glob_buffer);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->size);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_URLGlob(a, b, c) 	_unmarshall_struct_URLGlob(a, b, &c)

void _unmarshall_struct_State(char* buf, size_t* buf_index_, struct State** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    struct State* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->urlnum);
    unmarshall_string(buf, &buf_index, TEST_data_ref->uploadfile);
    unmarshall_struct_URLGlob(buf, &buf_index, TEST_data_ref->inglob);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->up);
    unmarshall_struct_URLGlob(buf, &buf_index, TEST_data_ref->urls);
    unmarshall_string(buf, &buf_index, TEST_data_ref->outfiles);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->li);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->infilenum);
    unmarshall_string(buf, &buf_index, TEST_data_ref->httpgetfields);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_struct_State(a, b, c) 	_unmarshall_struct_State(a, b, &c)

void ext_test_state_from_arg(struct extension_data data, struct State **s)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_struct_State(buf, &buf_index, *s);
}
struct extension_data ext_test_state_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
int ext_test_state_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
struct extension_data ext_test_state(struct extension_data data)
{
  struct State *s;
  ext_test_state_from_arg(data, &s);
  int return_value;
  return_value = test_state(s);
  struct extension_data result = ext_test_state_to_resp( return_value );
  return result;
}
struct extension_data ext_test_state_to_arg(struct State *s)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_struct_State(buf, &buf_index, s);
  data.bufc = buf_index;
  return data;
}
