import sys

sys.path.insert(0, '../../../../')

import function_serializer

output_file = sys.argv[1]

def InsertLines(lines, start, new_lines):
	new_lines = new_lines.split('\n')
	for i, new_line in enumerate(new_lines):
		lines.insert(start + i, new_lines[i])

interface_header_generated_code, interface_generated_code = function_serializer.RunSerializer(['../add_function/add_ten.h'], {'exclude_to_resp' : False, 'exclude_from_resp': False, 'exclude_ext_name': False, 'to_arg_name': None, 'from_arg_name': None, 'include_boilerplate': True})

interface_libcompart_code = '''
#include <string.h>
#include <stdlib.h>

#define NO_COMPARTS 2

static struct compart comparts[NO_COMPARTS] =
  {{.name = "hello compartment", .uid = 65534, .gid = 65534, .path = "/dev/"},
   {.name = "other compartment", .uid = 65534, .gid = 65534, .path = "/dev/"}};

struct extension_id *add_ten_ext = NULL;
'''
#add the libcompart code
interface = interface_libcompart_code + interface_header_generated_code + interface_generated_code

lines = interface.split('\n')

for idx, line in enumerate(lines):
	#update the code that the parent compartment checks:
	#struct extension_data ext_add_ten(struct extension_data data)
	#make a if the value is greater than ten and patch it
	if 'struct extension_data result = ext_' in line:
		new_lines = '''

//TODO fix, this is bug with libcompart not shutting down with return value of 0

if(return_value == (10 + 5)) { // 5 was the original value passed, and add_ten will add ten to it
	return_value = 9;
}
else {
	// it must enter in the above if condition if the value passed was correct. Otherwise it comes here, and the test failed
	return_value = 100;
}
'''
		InsertLines(lines, idx, new_lines)
		break

interface_code = '\n'.join(lines)

#write the interface code

with open(output_file, 'w') as f:
	f.write(interface_code)
