                              ___| | | |  _ \| |
                             / __| | | | |_) | |
                            | (__| |_| |  _ <| |___
                             \___|\___/|_| \_\_____|

 This is a patch to cURL to allow the compartmentalization of cURL functions.

 The purpose of this patch was to demonstrate libcompart's ability to be succesfully implanted in a large open source project.

 To install and run this patch follow the following steps!

 1. Clone this repo to your local machine.
 2. Install json-c with `sudo apt-get install libjson-c-dev`
 3. Run the [install script](patch_install.sh) on your local machine.
 4. Navigate into the cURL folder and run `make`.

 If all went well you should be able to run cURL from the 'src' folder, or run `make install` to install on your machine.

 Included in this branch are the source files for the compart_interface that are used to handle the coordination of cross compartment syncronization and communication.


## Patch Details for libcompart

The patch included will not only patch the cURL soure code, but will also touch on two files from libcompart.


### compart.c

```
> #define GENERAL_ARG_BUF_SIZE EXT_ARG_BUF_SIZE + 12
```

This is implmented to address an issue with the arguments not fully transmitting. Communication between the monitor and functional compartments are forced to be a maximum of 1024 even if the `ext_arg_buf_size` is set to a larger number.

In addition, `eid`s are packaged into the message data which take some space in the front of the data buffer, so additional space is required to allow the full message to transfer in libcompart with the eids.

```
>/* exit(EXIT_LOGGED_OFFSET + 37);*/
>exit(EXIT_OK);
```
This is implemented to for cURL unit tests. Inherently libcompart does not utilize the exit code of the program, instead assigning an exit code based on the communication between compartmnets. This impacted unit testing so was set to a fixed value to signify correct operation for cURL unit tests.

```
< #define MAX_COMPART_REGS 10
> #define MAX_COMPART_REGS 20
```
There are more than 10 registered functions that are required for curl operation.

```
<   /*if (getuid() != 0/*NOTE const -- uid for root*/) {
<     return false;
<   } */
```

cURL and, by extension, cURL unit test have spotty funcionality with root (depening on the operations being ran). This forces libcompart to run without root.

```
<     my_config = config;
>     /*my_config = config; */
>     my_config = default_config;
```

There are some issue with the configs causing a disconnect from the initialization of the compartment config to the initialization of the individual compartments. This manifests by setting the start_subs variable in the config to 0, which causes the program to hang as there are no other subcompartments available. This code overrides with the default setting.


### compart_api.h

```
< #define EXT_ARG_BUF_SIZE 512
> #define EXT_ARG_BUF_SIZE 2048
```

The serialized size of a cURL operation config is around 1188 bytes, which will not fit in a 512 byte buffer. It is therefore set to 2048.
