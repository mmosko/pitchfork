#Refactored out of struct_serialization.py - Henry Zhu 6/28/19

import sys
import pprint
import re
import argparse
import doctest

import libclang_config
import clang.cindex

# wrapper around a node that is part of a struct to fetch data from node easily
class StructMember():
	#struct reference is a reference to another node if the node is a pointer to said struct
	def __init__(self, node, struct_reference = None):
		self.node = node
		self.struct_reference = struct_reference

	def GetNode(self):
		return self.node

	def SetStructReference(self, struct_reference):
		#assert(isinstance(struct_reference, Struct))
		self.struct_reference = struct_reference

	def GetStructReference(self, structs):
		return structs.get(self.struct_reference)

	def GetDefinition(self):
		return self.node.get_definition()

	def GetType(self):
		definition = self.GetDefinition()
		type_spelling = definition.type.spelling
		return type_spelling

	def GetName(self):
		definition = self.GetDefinition()
		return definition.spelling

	def IsPointer(self):
		#TODO consider typedef to pointer?
		#TODO if pointer, why need type?
		definition = self.GetDefinition()
		type_spelling = self.GetType()
		return (definition.type.kind == clang.cindex.TypeKind.POINTER or type_spelling.find('*')) and '(*)' not in type_spelling

	def GetPointerLevel(self):
		if self.IsPointer():
			type_spelling = self.GetType()
			return type_spelling.count('*')
		return 0

	def GetBaseType(self):
		if self.IsPointer():
			type_spelling = self.GetType()
			index = type_spelling.rfind(' ')
                        if '(*)' in type_spelling: # account for function pointers
                            index = type_spelling.rfind(')') + 1
			return type_spelling[:index]
		return None

        def IsFunctionPointer(self):
		if self.IsPointer():
			type_spelling = self.GetType()
                        if '(*)' in type_spelling:
			    return True
		return False

# Wrapper around struct node (Contains struct members)
class Struct():
	def __init__(self, node, typedef = None):
		self.node = node
		self.typedef = typedef
		self.members = {}
                self.member_strings_ordered = []

	def GetNode(self):
		return self.node

	def GetDefinition(self):
		definition = None
		if self.typedef:
			definition = self.typedef.get_definition()
		else:
			definition = self.node.get_definition()
		return definition

	def GetName(self):
		return self.GetDefinition().spelling

	def GetType(self):
		return self.GetDefinition().type.spelling

	def GetBaseType(self):
		return self.node.canonical.get_definition().type.spelling

	def GetTypeNoSpace(self):
		return self.GetType().replace(" ", "_")

        def AddMember(self, member_string, member):
                self.members[member_string] = member
		self.member_strings_ordered.append(member_string)

	def GetMembers(self):
		return self.members
	
        def GetMemberStringsOrdered(self):
		return self.member_strings_ordered

	def __str__(self):
		return self.GetName()

	def __hash__(self):
		return hash(self.GetName())

	def __eq__(self, other):
	    if isinstance(other, Struct):
		return self.GetName() == other.GetName()
	    return False
